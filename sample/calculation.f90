module mod_cal
    implicit none
    contains
    subroutine calc_pi_number(n, n_in, nprocs) !円周率を計算するサブルーチン
        implicit none 
        integer :: n, n_mpi, n_in, i, nprocs 
        real(8) :: pi 

        n_mpi = n / nprocs

        n_in = 0 
        do i = 1, n_mpi 
            if(is_inside())n_in = n_in + 1 
        enddo
        
    end subroutine calc_pi_number

    subroutine calc_pi_all(n_in_tmp, n, pi,rooop)
        implicit none 
        integer :: n, n_in_tmp,rooop
        real(8) :: pi(:) 

        pi(rooop) = 4.0d0*dble(n_in_tmp)/dble(n) 
    end subroutine calc_pi_all

    function is_inside()
        implicit none
        logical :: is_inside
        real(8) :: pos(2),l2
        call random_number(pos)
        l2 = dsqrt(pos(1)*pos(1)+pos(2)*pos(2))
        is_inside = .false.
        if(l2<1.0d0) is_inside= .true.
    end function

end module mod_cal

! mpif90 -c calculation.f90
