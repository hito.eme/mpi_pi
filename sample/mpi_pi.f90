program mpi_pi
use mpi
use mod_pi

    implicit none

    !宣言部
    integer(4) :: i,n,ts(100),tf(100),t1(100),t2(100),t3(100),m,n_in,n_in_tmp,fin
    integer(4) :: t_all,t_hantei,t_tusin,t_cal,rooop
    real(8) :: pi(100),pi_ave

    !MPI宣言用
    integer(4) :: nprocs,myrank,ierr
    call MPI_INIT(ierr)
    call MPI_COMM_SIZE(MPI_COMM_WORLD, nprocs, ierr)
    call MPI_COMM_RANK(MPI_COMM_WORLD, myrank, ierr)
    write(*,*)"nprocs",nprocs 

    fin=10
    do rooop = 1,fin
        call system_clock(ts(rooop))
        call input_param(n)
        
        
        call system_clock(t1(rooop))    
        call calc_pi_number(n,n_in,nprocs)
        call system_clock(t2(rooop)) 
        m = 1
        call MPI_Allreduce(n_in, n_in_tmp, m, MPI_INTEGER8, MPI_SUM, MPI_COMM_WORLD, ierr)
        call system_clock(t3(rooop)) 
        call calc_pi_all(n_in_tmp, n, pi,rooop)
        
        call system_clock(tf(rooop))
    end do
    call pi_heikin(pi,pi_ave,fin)
    call timecount(tf,ts,t1,t2,t3,fin,t_all,t_hantei,t_tusin,t_cal)
    call output_pi_number(n,pi_ave,t_all,t_hantei,t_tusin,t_cal,nprocs,fin)


    call MPI_FINALIZE(ierr)

contains
    subroutine timecount(tf,ts,t1,t2,t3,fin,t_all,t_hantei,t_tusin,t_cal)
        integer(4),intent(in) :: tf(:),ts(:),t1(:),t2(:),t3(:),fin
        integer(4) :: i
        integer(4),intent(out) :: t_all,t_hantei,t_tusin,t_cal

        do i=1,fin
            t_all=(tf(i)-ts(i))+t_all
            t_hantei=(t2(i)-t1(i))+t_hantei
            t_tusin=(t3(i)-t2(i))+t_tusin
        end do

    end subroutine
    subroutine pi_heikin(pi,pi_ave,fin)
        real(8),intent(in) :: pi(:)
        real(8),intent(out) :: pi_ave
        integer(4) :: i,fin
        do i=1,fin
            pi_ave=pi(i)+pi_ave
        end do
    end subroutine


end program mpi_pi
! mpif90 mpi_pi.f90 io.o calculation.o pi.o 
!gfortran -I home/cll18700/mpi_pi/src/main mpi_pi.f90 pi.o 