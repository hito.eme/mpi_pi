module mod_io
    implicit none
    contains
    
    subroutine input_param (n)
        implicit none
        integer(4),intent(out) :: n

        !n= 10000000

        open(10,file="input.dat",status="old")
        read(10,*)n
        close(10)        

    end subroutine input_param
    

    subroutine output_pi_number(n,pi_ave,t_all,t_hantei,t_tusin,t_cal,nprocs,fin)
        implicit none
        integer(4),intent(in) :: n,t_all,t_hantei,t_tusin,t_cal,nprocs,fin
        real(8),intent(in) :: pi_ave

        write(*,*)"n",n
        write(*,*)"pi",pi_ave
        open(110,file="output.dat",status="replace")
        write(110,*)"n",n
        write(110,*)"pi",(pi_ave/fin)
        write(110,*)"time all",(t_all/fin)
        write(110,*)"time hantei",(t_hantei/fin)
        write(110,*)"time tuusin",(t_tusin/fin)

        write(110,*)"nprocs",nprocs
        close(110)
    end subroutine output_pi_number

end module mod_io

!mpif90 -c io.f90