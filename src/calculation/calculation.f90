module mod_cal
    implicit none
    contains
    subroutine calc_pi_number(n,n_in,nprocs)
        implicit none
        integer(4),intent(in) :: n,nprocs
        integer(4) :: i,n_mpi
        integer(4),intent(out) :: n_in
        real(8),intent(out) :: pi
        
        !割り切れない場合を検討
        n_mpi=n/nprocs
        
        n_in=0
        do i=1,n_mpi
            if(is_inside()) n_in = n_in +1
        end do
        


    end subroutine calc_pi_number

    subroutine calc_pi_all(n_in_tmp, n, pi)
        implicit none
        integer(4),intent(in) :: n, n_in_tmp !変数n,i,n_inを整数型で宣言
        real(8),intent(out) :: pi

        pi = 4.0d0*dble(n_in_tmp)/dble(n)

    end subroutine

    function is_inside()
        implicit none
        logical :: is_inside
        real(8) :: pos(2),l2
        call random_number(pos)
        l2 = dsqrt(pos(1)*pos(1)+pos(2)*pos(2))
        is_inside = .false.
        if(l2<1.0d0) is_inside= .true.
    end function

end module mod_cal

! gfortran -c calculation.f90
