module mod_io
    implicit none
    contains
    
    subroutine input_param(n)
        implicit none
        integer(4),intent(out) :: n

        !n= 10000000

        open(10,file="../inoutput/input.dat",status="old")
        read(10,*)n
        close(10)        

    end subroutine input_param

    subroutine output_pi_number(n,pi,tf,ts)
        implicit none
        integer(4),intent(in) :: n,tf,ts
        real(8),intent(in) :: pi

        write(*,*)"n",n
        write(*,*)"pi",pi
        open(110,file="../inoutput/output.dat",status="replace")
        write(110,*)"n",n
        write(110,*)"pi",pi
        write(110,*)"time",tf-ts
        close(110)
    end subroutine output_pi_number

end module mod_io

!gfortran -c io.f90